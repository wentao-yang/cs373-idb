import React from 'react';
import { useHistory } from 'react-router-dom';
import MaterialTable from 'material-table';

function SearchResults ( {data} ) {
    const history = useHistory();
    const routeChange = (pathName, rowData) =>{ 
      let path = pathName;
      history.push({
        pathname: path,
        state: { data: rowData},
      });
    }
    let artistList = {
      columns: [
        { title: 'Artist Name', field: 'artist_name' },
        { title: 'ID', field: 'artist_id', type:  'numeric' },
        { title: 'Genre', field: 'artist_genres[0]' },
      ],
      data: data["artists"],
    }
    let albumList = {
      columns: [
        { title: 'Album Name', field: 'album_name' },
        { title: 'Number of Tracks', field: 'album_numtrack', type:  'numeric' },
        { title: 'Release Date', field: 'album_release_day' },
      ],
      data: data["albums"],
    }
    let trackList = {
      columns: [
        { title: 'Track Name', field: 'track_name' },
        { title: 'Artist', field: 'track_artists[0].artist_name' },
        { title: 'Release Date', field: 'track_release_day' },
      ],
      data: data["tracks"],
    }
    let display = <div>
      <MaterialTable
        title="Artists"
        columns={artistList.columns}
        data={artistList.data}
        onRowClick={(event, rowData) => {
          routeChange("/artistInstance", rowData);
        }}
      />
      <MaterialTable
        title="Albums"
        columns={albumList.columns}
        data={albumList.data}
        onRowClick={(event, rowData) => {
          routeChange("/albumInstance", rowData);
        }}
      />
      <MaterialTable
        title="Tracks"
        columns={trackList.columns}
        data={trackList.data}
        onRowClick={(event, rowData) => {
          routeChange("/trackInstance", rowData);
        }}
      />
    </div>
  
    return display;
  }

  export default SearchResults;